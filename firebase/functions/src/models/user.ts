export enum UserRole {
  NORMAL = "normal",
  ADMIN = "admin",
}

export type UserDocument = {
  role: UserRole;
};
