import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { UserDocument, UserRole } from "./models/user";

admin.initializeApp();

const db = admin.firestore();

const region = "europe-central2";

const users = "users";

export const onAccountCreate = functions
  .region(region)
  .auth.user()
  .onCreate(async (user) => {
    const userDocument: UserDocument = {
      role: UserRole.NORMAL,
    };
    await db.collection(users).doc(user.uid).create(userDocument);
  });

export const onAccountDelete = functions
  .region(region)
  .auth.user()
  .onDelete(async (user) => {
    await db.collection(users).doc(user.uid).delete();
  });

export const onUserDocumentWrite = functions
  .region(region)
  .firestore.document(`${users}/{userId}`)
  .onWrite(async (change, context) => {
    const uid = context.params.userId;
    const data = change.after.data();
    await admin.auth().setCustomUserClaims(uid, data ?? null);
  });
