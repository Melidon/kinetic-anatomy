import '../enum_values.dart';

class Muscle {
  Muscle({
    required this.name,
    required this.image,
    required this.muscleLocation,
    required this.muscleParts,
    required this.innervations,
  });

  String name;
  String image;
  MuscleLocation muscleLocation;
  List<MusclePart> muscleParts;
  List<String> innervations;

  factory Muscle.fromJson(Map<String, dynamic> json) => Muscle(
        name: json["name"],
        image: json["image"],
        muscleLocation: muscleLocationValues.map[json["muscleLocation"]]!,
        muscleParts: List<MusclePart>.from(json["muscleParts"].map((x) => MusclePart.fromJson(x))),
        innervations: List<String>.from(json["innervations"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "image": image,
        "muscleLocation": muscleLocationValues.reverse[muscleLocation],
        "muscleParts": List<dynamic>.from(muscleParts.map((x) => x.toJson())),
        "innervations": List<dynamic>.from(innervations.map((x) => x)),
      };
}

enum MuscleLocation { UPPER_LIMB, LOWER_LIMB, TORSO, FACE }

final muscleLocationValues = EnumValues({
  "face": MuscleLocation.FACE,
  "lowerLimb": MuscleLocation.LOWER_LIMB,
  "torso": MuscleLocation.TORSO,
  "upperLimb": MuscleLocation.UPPER_LIMB,
});

class MusclePart {
  MusclePart({
    required this.name,
    required this.origin,
    required this.insertion,
    required this.functions,
  });

  String name;
  String origin;
  String insertion;
  List<String> functions;

  factory MusclePart.fromJson(Map<String, dynamic> json) => MusclePart(
        name: json["name"],
        origin: json["origin"],
        insertion: json["insertion"],
        functions: List<String>.from(json["functions"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "origin": origin,
        "insertion": insertion,
        "functions": List<dynamic>.from(functions.map((x) => x)),
      };
}
