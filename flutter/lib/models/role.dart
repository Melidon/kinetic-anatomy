import '../enum_values.dart';

enum Role { NORMAL, ADMIN }

final roleValues = EnumValues({
  "normal": Role.NORMAL,
  "admin": Role.ADMIN,
});
