import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/muscle.dart';

const muscles = 'muscles';

class MuscleService {
  static CollectionReference<Muscle> get _collection {
    return FirebaseFirestore.instance.collection(muscles).withConverter<Muscle>(
          fromFirestore: (snapshot, _) => Muscle.fromJson(snapshot.data()!),
          toFirestore: (muscle, _) => muscle.toJson(),
        );
  }

  static Future<DocumentSnapshot<Muscle>> get(String id) {
    return _collection.doc(id).get();
  }

  static Future<List<DocumentSnapshot<Muscle>>> list() {
    return _collection.get().then((query) => query.docs);
  }

  static Future<List<DocumentSnapshot<Muscle>>> listWhereMuscleLocationIn(List<MuscleLocation> muscleLocations) {
    final muscleLocationStrings = muscleLocations.map((muscleLocation) => muscleLocationValues.reverse[muscleLocation]).toList();
    return _collection.where("muscleLocation", whereIn: muscleLocationStrings).get().then((query) => query.docs);
  }

  static Future<DocumentSnapshot<Muscle>> create(Muscle muscle) {
    return _collection.add(muscle).then((muscleRef) => muscleRef.get());
  }

  static Future<void> update(String id, Muscle muscle) {
    return _collection.doc(id).set(muscle);
  }

  static Future<void> delete(String id) {
    return _collection.doc(id).delete();
  }
}
