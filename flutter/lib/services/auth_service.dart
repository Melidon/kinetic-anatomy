import 'package:firebase_auth/firebase_auth.dart';

import '../models/role.dart';

class AuthService {
  static Future<Role?> getRole() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser == null) {
      return null;
    }
    final idTokenResult = await currentUser.getIdTokenResult();
    final String? roleString = idTokenResult.claims?["role"];
    if (roleString == null) {
      return null;
    }
    final role = roleValues.map[roleString];
    return role;
  }
}
