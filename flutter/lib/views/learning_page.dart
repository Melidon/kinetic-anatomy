import 'package:flutter/material.dart';
import 'package:swipe/swipe.dart';

import '../models/muscle.dart';
import '../services/muscle_service.dart';

class LearningPage extends StatelessWidget {
  const LearningPage(this.muscleLocations, {super.key});

  final List<MuscleLocation> muscleLocations;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: MuscleService.listWhereMuscleLocationIn(muscleLocations),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final muscles = snapshot.data!.map((muscle) => muscle.data()).whereType<Muscle>().toList();
          if (muscles.isEmpty) {
            // TODO: Make it look better!
            return const Text('Egy izom sem található még itt.');
          }
          return LearningPageContent(muscles);
        }
        if (snapshot.hasError) {
          // TODO: Make it look better!
          return Text('Error: ${snapshot.error}');
        }
        // TODO: Make it look better!
        return const CircularProgressIndicator();
      },
    );
  }
}

class LearningPageContent extends StatefulWidget {
  const LearningPageContent(this.muscles, {super.key});

  final List<Muscle> muscles;

  @override
  State<LearningPageContent> createState() => _LearningPageContentState();
}

class _LearningPageContentState extends State<LearningPageContent> {
  // TODO: Increment when swiping up.
  int _muscleIndex = 0;

  static const List<Tab> _tabs = <Tab>[
    Tab(text: 'Kép'),
    Tab(text: 'Eredés'),
    Tab(text: 'Tapadás'),
    Tab(text: 'Beidegződés'),
    Tab(text: 'Funkció'),
  ];

  @override
  Widget build(BuildContext context) {
    final muscle = widget.muscles[_muscleIndex];

    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(muscle.name),
          bottom: const TabBar(
            tabs: _tabs,
          ),
        ),
        body: Swipe(
          onSwipeUp: () {
            if (_muscleIndex < widget.muscles.length - 1) {
              setState(() {
                _muscleIndex += 1;
              });
            } else {
              const snackBar = SnackBar(
                content: Text('A végére értél, nincs tovább!'),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
          onSwipeDown: () {
            if (_muscleIndex > 0) {
              setState(() {
                _muscleIndex -= 1;
              });
            } else {
              const snackBar = SnackBar(
                content: Text('Ez az eleje, nincsen feljebb!'),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
          child: TabBarView(
            children: [
              _MuscleImageView(muscle),
              _MuscleOriginView(muscle),
              _MuscleInsertionView(muscle),
              _MuscleInnervationsView(muscle),
              _MuscleFunctionsView(muscle),
            ],
          ),
        ),
      ),
    );
  }
}

class _MuscleImageView extends StatelessWidget {
  const _MuscleImageView(this.muscle, {super.key});

  final Muscle muscle;

  @override
  Widget build(BuildContext context) {
    // TODO: Make it look better!
    return Image.network(muscle.image);
  }
}

class _MuscleOriginView extends StatelessWidget {
  const _MuscleOriginView(this.muscle, {super.key});

  final Muscle muscle;

  @override
  Widget build(BuildContext context) {
    // TODO: Make it look better!
    return ListView.builder(
      itemCount: muscle.muscleParts.length,
      itemBuilder: (context, index) {
        final musclePart = muscle.muscleParts[index];
        return Text('${musclePart.name}: ${musclePart.origin}');
      },
    );
  }
}

class _MuscleInsertionView extends StatelessWidget {
  const _MuscleInsertionView(this.muscle, {super.key});

  final Muscle muscle;

  @override
  Widget build(BuildContext context) {
    // TODO: Make it look better!
    return ListView.builder(
      itemCount: muscle.muscleParts.length,
      itemBuilder: (context, index) {
        final musclePart = muscle.muscleParts[index];
        return Text('${musclePart.name}: ${musclePart.insertion}');
      },
    );
  }
}

class _MuscleInnervationsView extends StatelessWidget {
  const _MuscleInnervationsView(this.muscle, {super.key});

  final Muscle muscle;

  @override
  Widget build(BuildContext context) {
    // TODO: Make it look better!
    return ListView.builder(
      itemCount: muscle.innervations.length,
      itemBuilder: (context, index) => Text(muscle.innervations[index]),
    );
  }
}

class _MuscleFunctionsView extends StatelessWidget {
  const _MuscleFunctionsView(this.muscle, {super.key});

  final Muscle muscle;

  @override
  Widget build(BuildContext context) {
    // TODO: Make it look better!
    return ListView.builder(
      itemCount: muscle.muscleParts.length,
      itemBuilder: (context, index) {
        final musclePart = muscle.muscleParts[index];
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('${musclePart.name}:'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: musclePart.functions.map((function) => Text(' - $function')).toList(),
            ),
          ],
        );
      },
    );
  }
}
