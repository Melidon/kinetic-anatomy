import 'package:flutter/material.dart';
import 'package:kinetic_anatomy/services/muscle_service.dart';

class MuscleEditor extends StatelessWidget {
  const MuscleEditor({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Izmok szerkesztése'),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Izom hozzáadása',
        child: const Icon(Icons.add),
        onPressed: () {
          // TODO: Show dialog for adding a muscle.
        },
      ),
      body: FutureBuilder(
        future: MuscleService.list(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final muscles = snapshot.data!;
            return ListView.builder(
              itemCount: muscles.length,
              itemBuilder: (context, index) {
                final muscleId = muscles[index].id;
                final muscle = muscles[index].data()!;
                // TODO: Make it look better!
                return Row(
                  children: [
                    Text(muscle.name),
                    IconButton(
                      tooltip: 'Izom szerkesztése',
                      onPressed: () {
                        // TODO: Show dialog for editing muscle.
                      },
                      icon: const Icon(Icons.edit),
                    ),
                    IconButton(
                      tooltip: 'Izom törlése',
                      onPressed: () {
                        // TODO: Show confirm dialog and than call:
                        MuscleService.delete(muscleId);
                      },
                      icon: const Icon(Icons.delete),
                    ),
                  ],
                );
              },
            );
          }
          if (snapshot.hasError) {
            // TODO: Make it look better!
            return Text('Error: ${snapshot.error}');
          }
          // TODO: Make it look better!
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
