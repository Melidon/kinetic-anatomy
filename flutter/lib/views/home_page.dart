import 'package:flutter/material.dart';
import 'package:flutterfire_ui/auth.dart';
import 'package:kinetic_anatomy/models/role.dart';
import 'package:kinetic_anatomy/services/auth_service.dart';

import '../models/muscle.dart';
import 'learning_page.dart';
import 'muscle_editor.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kinetic Anatomy'),
        actions: <Widget>[
          FutureBuilder(
            future: AuthService.getRole(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null && snapshot.data! == Role.ADMIN) {
                return IconButton(
                  tooltip: 'Izmok szerkesztése',
                  icon: const Icon(Icons.edit),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const MuscleEditor();
                      },
                    ));
                  },
                );
              }
              return Container();
            },
          ),
          IconButton(
            tooltip: 'Fiók kezelése',
            icon: const Icon(Icons.person),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return ProfileScreen(
                    appBar: AppBar(
                      title: const Text('Fiók kezelése'),
                    ),
                    actions: [
                      SignedOutAction((BuildContext context) {
                        Navigator.pop(context);
                      }),
                    ],
                  );
                },
              ));
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return const LearningPage([MuscleLocation.UPPER_LIMB]);
                },
              ));
            },
            child: Container(
              color: const Color.fromARGB(255, 252, 183, 161),
              width: 256,
              child: const Center(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Text(
                    'Felső végtag',
                    style: TextStyle(
                      fontSize: 32,
                    ),
                  ),
                ),
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return const LearningPage([MuscleLocation.LOWER_LIMB]);
                },
              ));
            },
            child: Container(
              color: const Color.fromARGB(255, 242, 143, 177),
              width: 256,
              child: const Center(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Text(
                    'Alsó végtag',
                    style: TextStyle(
                      fontSize: 32,
                    ),
                  ),
                ),
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return const LearningPage([MuscleLocation.TORSO, MuscleLocation.FACE]);
                },
              ));
            },
            child: Container(
              color: const Color.fromARGB(255, 228, 236, 156),
              width: 256,
              child: const Center(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Text(
                    'Törzs és arc',
                    style: TextStyle(
                      fontSize: 32,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
